import * as PIXI from 'pixi.js';
import { Character, Peng } from './lib/chars';
import { Stage } from './lib/stage';

const loader = new PIXI.Loader;
const ticker = PIXI.Ticker.shared;

loader.add('peng', '/assets/peng/peng.jpg');

const app = new PIXI.Application({
    width: window.innerWidth,
    height: window.innerHeight
});
document.body.appendChild(app.view);

loader.load( (loader, resources) => {
    const container = new Stage(app, 'Main');

    let peng = new Character(app, {
        resource: resources.peng.texture
    });
    container.stage(peng);

    let peng2 = new Peng(app, {
        resource: resources.peng.texture,
    });
    container.stage(peng2);
});