import _ from 'lodash';
import * as PIXI from 'pixi.js';
import { behave } from './behave';

let index = 0;

export class Character {
  constructor(app, config) {
    this.index = index++;
    this.config = config;
    this.sprite = PIXI.Sprite.from(config.resource);
    this.sprite.anchor.set(0.5);
    this.sprite.interactive = true;
    this.sprite.buttonMode = true;
    this.sprite.on('mousedown', event => {
      this.click(event);
    });

    this.sprite.x = (_.has(config, 'x')) ? _.get(config, 'x', 0) : Math.random() * app.screen.width;
    this.sprite.y = (_.has(config, 'y')) ? _.get(config, 'y', 0) : Math.random() * app.screen.height;
  }


  stage = (app, stage) => {
    this.stage = stage;
    stage.addChild(this.sprite);
  }

  click = event => {
    console.log('Clicked', this.index, this.config);
  };

  index = () => {
    return this.index;
  }

  bound = (x, y) => {
    return false;
  }

  coords = () => {
    return {
      x : this.sprite.x,
      y: this.sprite.y
    }
  }

}
