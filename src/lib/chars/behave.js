export const behaviours = {
  'STILL':0,
  'WOBBLE':1,
  'GOTO':2
}

export const behave = {
  0 : (memory, x, y) => [memory, x, y],
  1 : (memory, x, y) => {
    return [
      memory,
      x += Math.round((Math.random() * 4) - 2),
      y += Math.round((Math.random() * 4) - 2)
    ]
  },
  2 : (memory, x, y) => {
    let newX = x;
    let newY = y;
    if (x > memory.target.x) {
      newX--;
    } else {
      newX++;
    }

    if (y > memory.target.y) {
      newY--;
    } else {
      newY++;
    }

    return [
      memory,
      newX,
      newY
    ]
  }
};