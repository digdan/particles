import * as PIXI from 'pixi.js';
import _ from 'lodash';

export class Stage {
  constructor(app, name) {
    this.app = app;
    this.drag = null;
    this.dragOffset = null;
    this.container = new PIXI.Container(10000, {
      scale: true,
      position: true,
      rotation: true,
      uvs: true,
      alpha: true,
    });
    this.container.name = name;
    this.container.interactive = true;
    this.container.containsPoint = () => true;
    this.container.on('mousedown', e => {
      if (this.drag === null && e.target.name === 'Main') {
        this.drag = e.data;
        this.dragOffset = _.clone({
          x: e.data.global.x - this.container.position.x,
          y: e.data.global.y - this.container.position.y
        });
      }
    });
    this.container.on('mouseup', e => {
      this.drag = null;
      this.dragOffset = null;
    })
    this.container.on('mousemove', e => {
      if (this.drag !== null) {
        const newPosition = this.drag.getLocalPosition(this.container.parent);
        const calculatedNewPosition = {
          x : ( newPosition.x - this.dragOffset.x ),
          y : ( newPosition.y - this.dragOffset.y )
        }
        if (!this.isBound(calculatedNewPosition)) {
          this.container.position.x = calculatedNewPosition.x;
          this.container.position.y = calculatedNewPosition.y;
        }
      }
    });

    app.stage.addChild(this.container);
  }

  stage = character => {
    character.stage(this.app, this.container);
  }

  isBound = coord => {
    if (coord.x < -1000) return true;
    if (coord.x > 1000) return true;
    if (coord.y < -1000) return true;
    if (coord.y > 1000) return true;
    return false;
  }

}