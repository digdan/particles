# Build Spritesheets
base=`dirname $0`;
array=($base/sprites/*/)
for dir in "${array[@]}"; do 
	spritename=`basename $dir`;
  mkdir -p $base/spritesheets/$spritename;
  npx spritesheet-js $dir/*.png -n $spritename -p $base/spritesheets/$spritename;
done
